"use strict";

var app = angular.module("app", ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when("/products", {
            templateUrl: "views/product-list.htm",
            controller: "ProductListCtrl"
        })
        .when("/cart", {
            templateUrl: "views/cart-list.htm",
            controller: "CartListCtrl"
        })
        .otherwise({
            redirectTo: "/products"
        })
}]);

app.factory("ProductService", function() {
    var products = [{
        imgUrl: "aba-06.png",
        name: "Affliction",
        price: 500,
        rating: 10,
        binding: "pocket, bound",
        publisher: "Book Satan",
        releaseDate: "2002-03-04",
        details: "Affliction my renumeration for living. I'll never escape this world alive."
    }, {
        imgUrl: "aba-06.png",
        name: "The carver",
        price: 200,
        rating: 6,
        binding: "pocket, bound",
        publisher: "Book devil",
        releaseDate: "2013-03-04",
        details: "He carves code like no other, is he a wizard?"
    }, {
        imgUrl: "enchantment.jpg",
        name: "The grinder",
        price: 204,
        rating: 7,
        binding: "pocket, bound",
        publisher: "Book saint",
        releaseDate: "2015-03-04",
        details: "the path to enligthenment is paved with pain."
    }, {
        imgUrl: "aba-06.png",
        name: "Slapstick brothers",
        price: 200,
        rating: 6,
        binding: "pocket, bound",
        publisher: "Book devil",
        releaseDate: "2013-03-07",
        details: "Humor has never been this fun! You will die of laughter."
    }];
});

app.controller('HeaderCtrl', ['$scope', '$location',
    function($scope, $location) {
        $scope.appDetails = {
            title: "BooKart",
            tagline: "We have 1 millon books!"
        }

        $scope.getClass = function(path) {
            return ($location.path().substr(0, path.length) === path) ? 'active' : '';
        }
    }
]);

app.controller('ProductListCtrl', ['$scope',
    function($scope) {
        $scope.products = [{
            imgUrl: "aba-06.png",
            name: "Affliction",
            price: 500,
            rating: 10,
            binding: "pocket, bound",
            publisher: "Book Satan",
            releaseDate: "2002-03-04",
            details: "Affliction my renumeration for living. I'll never escape this world alive."
        }, {
            imgUrl: "aba-06.png",
            name: "The carver",
            price: 200,
            rating: 6,
            binding: "pocket, bound",
            publisher: "Book devil",
            releaseDate: "2013-03-04",
            details: "He carves code like no other, is he a wizard?"
        }, {
            imgUrl: "enchantment.jpg",
            name: "The grinder",
            price: 204,
            rating: 7,
            binding: "pocket, bound",
            publisher: "Book saint",
            releaseDate: "2015-03-04",
            details: "the path to enligthenment is paved with pain."
        }, {
            imgUrl: "aba-06.png",
            name: "Slapstick brothers",
            price: 200,
            rating: 6,
            binding: "pocket, bound",
            publisher: "Book devil",
            releaseDate: "2013-03-07",
            details: "Humor has never been this fun! You will die of laughter."
        }];

        $scope.addToCart = function(product) {
            console.log("Added to cart " + JSON.stringify(product));
        }
    }
]);

app.controller('CartListCtrl', ['$scope',
    function($scope) {
        $scope.cart = [];

        $scope.buy = function(product) {
            console.log("Buy: " + JSON.stringify(product));
        }
    }
]);
